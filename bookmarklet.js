(async function (instanceOrigin) {
    const inputEls = Array.from(document.querySelectorAll('input[type=password]'));
    const hasSingleInput = inputEls.length === 1;

    if (!hasSingleInput) {
        if (!window.confirm('Unable to find a single password input. Continue and use your clipboard instead?')) return;
    }

    const channel = new MessageChannel();
    const inputEl = inputEls[0];
    const t = (window.screen.height / 2) - 200;
    const l = (window.screen.width / 2) - 150;
    const w = window.open(instanceOrigin + '?origin=' + location.origin, 'PassmanModal', 'popup,width=300,height=400,left=' + l + ',top=' + t);
    w.focus();

    function once(t, e, h) {
        const hh = async (ee) => { await h(ee); t.removeEventListener(e, hh); };
        t.addEventListener(e, hh);
    }

    setTimeout(() => w.postMessage('', '*', [channel.port2]), 1000);

    function onMessage(e) {
        let pass = e.data;
        e.data = null;

        if (hasSingleInput) {
            inputEl.value = pass;
            pass = null;
        } else {
            once(window, 'focus', async () => {
                await navigator.clipboard.writeText(pass);
                pass = null;
            });
        }

        channel.port1.close();
    }
    channel.port1.onmessage = onMessage;
})($$instanceOrigin)