tr -d '\n' < ./bookmarklet.js | tr -s ' ' > dist-bookmarklet.js
awk 'FNR==NR{s=(!s)?$0:s RS $0;next} /<<bookmarklet>>/{sub(/<<bookmarklet>>/, s)} 1' dist-bookmarklet.js ./index.html > ./public/index.html
npx serve ./public